<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Workshop
 * @package App
 *
 * @property int $id
 * @property Carbon $day
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read WorkshopTime[] $workshopTimes
 * @property-read Customer[] $customers
 */
class Workshop extends Model
{
    protected $fillable = [
        'day'
    ];

    protected $casts = [
        'day' => 'date:M d'
    ];

    public function workshopTimes()
    {
        return $this->hasMany(WorkshopTime::class, 'workshop_id');
    }
}

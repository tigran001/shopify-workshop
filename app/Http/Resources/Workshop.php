<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Workshop extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'day' => $this->day->format('M d'),
            'is_available' => $this->workshopTimes->sum(function ($workshopTime) {
                return $workshopTime->customers->count();
            }) < $this->workshopTimes->sum('max_num_of_guests'),
            'workshop_times' => $this->whenLoaded('workshopTimes', WorkshopTime::collection($this->workshopTimes))
        ];
    }
}

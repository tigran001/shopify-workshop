<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WorkshopTime extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'time' => $this->time,
            'is_available' => $this->max_num_of_guests > $this->customers->count(),
            'available_spots' => $this->max_num_of_guests - $this->customers->count()
        ];
    }
}

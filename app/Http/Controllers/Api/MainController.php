<?php


namespace App\Http\Controllers\Api;


use App\Customer;
use App\CustomerWorkshop;
use App\Facades\Shopify;
use App\Http\Controllers\Controller;
use App\Http\Resources\Workshop;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return Workshop::collection(\App\Workshop::all());
    }

    public function searchCustomers(Request $request)
    {
        return Shopify::getCustomersByQuery($request->customer);
    }

    public function store(Request $request)
    {
        $customer = new Customer();
        $customer->name = $request->customer['name'];
        $customer->phone = $request->customer['phone'];
        $customer->is_leader = true;
        $customer->save();

        $customerWorkshop = new CustomerWorkshop();
        $customerWorkshop->customer_id = $customer->id;
        $customerWorkshop->workshop_time_id = $request->workshop_time_id;
        $customerWorkshop->save();

        foreach ($request->guests as $guest) {
            $guest = new Customer();
            $guest->name = $guest['name'];
            $guest->email = $guest['email'];
            $guest->save();

            $customerWorkshop = new CustomerWorkshop();
            $customerWorkshop->customer_id = $guest->id;
            $customerWorkshop->workshop_time_id = $request->workshop_time_id;
            $customerWorkshop->save();
        }
    }
}

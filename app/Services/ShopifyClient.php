<?php


namespace App\Services;


use GuzzleHttp\Client;

class ShopifyClient
{
    public $client;

    public function __construct()
    {
        $config = config('services.shopify');
        $baseUri = $config['domain'] . '/admin/api/' . $config['api_version'] . '/';
        $this->client = new Client([
            'base_uri' => $baseUri,
            'headers' => [
                'X-Shopify-Access-Token' => $config['api_password']
            ]
        ]);
    }

    public function getCustomersByQuery(string $name = null)
    {
        try {
            $response = $this->client->get("customers/search.json?query={$name}");

            return $response->getBody()->getContents();
        } catch (\Exception $e) {

        }
    }
}

<?php


namespace App\Facades;


use App\Services\ShopifyClient;
use Illuminate\Support\Facades\Facade;

/**
 * Class Shopify
 * @package App\Facades
 *
 * @method static array getCustomersByQuery(string $name = null)
 */
class Shopify extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ShopifyClient::class;
    }
}

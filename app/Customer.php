<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property boolean $is_leader
 * @property string $email
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read Workshop $workshopTime
 */
class Customer extends Model
{
    protected $fillable = [
        'name',
        'phone',
        'is_leader',
        'email'
    ];

    public function workshopTime()
    {
        return $this->belongsToMany(Workshop::class);
    }
}

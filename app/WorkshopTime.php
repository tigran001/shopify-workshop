<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WorkshopTime
 * @package App
 *
 * @property int $id
 * @property int $workshop_id
 * @property string $time
 * @property int $max_num_of_guests
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read Workshop $workshop
 */
class WorkshopTime extends Model
{
    protected $fillable = [
        'workshop_id',
        'time',
        'max_num_of_guests'
    ];

    public function workshop()
    {
        return $this->belongsTo(Workshop::class, 'workshop_id');
    }

    public function customers()
    {
        return $this->belongsToMany(Customer::class, 'customer_workshops');
    }
}

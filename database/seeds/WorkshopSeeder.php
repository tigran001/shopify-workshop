<?php

use Illuminate\Database\Seeder;

class WorkshopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workshops = [
            [
                'day' => '2020-06-01',
                'workshopTimes' => [
                    [
                        'time' => '9AM - 12PM',
                        'max_num_of_guests' => 5
                    ],
                    [
                        'time' => '12PM - 3PM',
                        'max_num_of_guests' => 5
                    ],
                    [
                        'time' => '3PM - 6PM',
                        'max_num_of_guests' => 7
                    ],
                ]
            ],
            [
                'day' => '2020-06-02',
                'workshopTimes' => [
                    [
                        'time' => '9AM - 12PM',
                        'max_num_of_guests' => 5
                    ],
                    [
                        'time' => '12PM - 3PM',
                        'max_num_of_guests' => 5
                    ],
                    [
                        'time' => '3PM - 6PM',
                        'max_num_of_guests' => 12
                    ],
                ]
            ],
            [
                'day' => '2020-06-03',
                'workshopTimes' => [
                    [
                        'time' => '9AM - 12PM',
                        'max_num_of_guests' => 5
                    ],
                    [
                        'time' => '12PM - 3PM',
                        'max_num_of_guests' => 7
                    ],
                    [
                        'time' => '3PM - 6PM',
                        'max_num_of_guests' => 5
                    ],
                ]
            ],
        ];

        foreach ($workshops as $workshop) {
            $newWorkshop = \App\Workshop::create(['day' => $workshop['day']]);

            foreach ($workshop['workshopTimes'] as $workshopTime) {
                $newWorkshop->workshopTimes()->create($workshopTime);
            }
        }
    }
}

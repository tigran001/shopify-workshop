@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <book-workshop></book-workshop>
    </div>
</div>
@endsection
